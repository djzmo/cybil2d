#ifndef CYBIL_WINDOW_WINDOW_H
#define CYBIL_WINDOW_WINDOW_H

#include <functional>
#include "../window_configuration.h"
#include "../events.h"
#include "../draw_target.h"

namespace cybil
{

typedef void(*EventFunction)(const Event &);

class WindowProtocol : public DrawTarget
{
protected:
    EventFunction _eventFunction;
    WindowConfiguration _windowConfig;
    bool _isOpen, _isAcceptingMouseMovedEvents, _isEmulatingSingleTouch;
	int _frameRate;

    virtual void create(const WindowConfiguration &configuration) = 0;
    virtual void destroy() = 0;

    virtual ~WindowProtocol();
public:
    WindowProtocol();
    WindowProtocol(const WindowConfiguration &configuration);

    bool isOpen();
    virtual void start() = 0;
	virtual void stop() = 0;

    const WindowConfiguration &getConfiguration() const;
    virtual void setEventFunction(EventFunction f);
    const EventFunction &getEventFunction() const;
	void setAcceptsMouseMovedEvents(bool flag);
	bool isAcceptingMouseMovedEvents();
	void setEmulateSingleTouch(bool flag);
	bool isEmulatingSingleTouch();
	void setFrameRate(int frameRate);
	int getFrameRate();
	const Size &getSurfaceSize() const;
};
}

#endif
