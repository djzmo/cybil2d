#ifndef CYBIL_OS_WIN32_WINDOW_H
#define CYBIL_OS_WIN32_WINDOW_H

#include <cybil/os.h>

#ifdef CYBIL_TARGET_WIN32

#include <Windows.h>
#include <chrono>
#include <cybil/os/window_protocol.h>
#include <cybil/gl.h>

namespace cybil
{
	class Window : public WindowProtocol
	{
		HWND _window;
		HDC _windowDc;
		HGLRC _oglContext;
		int _emulatedTouchId;

		std::chrono::time_point<std::chrono::high_resolution_clock> _lastTime;

	protected:
		void create(const WindowConfiguration &configuration);
		void destroy();

		static LRESULT CALLBACK proxyWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
		LRESULT CALLBACK windowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	public:
		Window();
		Window(const WindowConfiguration &configuration);
		~Window();

		void start();
		void stop();
	};
}

#endif

#endif
