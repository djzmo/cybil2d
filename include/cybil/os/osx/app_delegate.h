#include "../../os.h"

#ifdef CYBIL_TARGET_OSX
#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject<NSApplicationDelegate, NSWindowDelegate>
{
    NSWindow *window;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender;
- (NSWindow *)window;
- (void)setWindow:(NSWindow *)window;

@end

#endif