#ifndef CYBIL_OS_OSX_WINDOW_H
#define CYBIL_OS_OSX_WINDOW_H

#include "../../os.h"

#ifdef CYBIL_TARGET_OSX
#import <Cocoa/Cocoa.h>
#include "../window_protocol.h"
#include "gl_view.h"
#include "app_delegate.h"

namespace cybil
{
class Drawable;
class Window : public WindowProtocol
{
    NSAutoreleasePool *_autoreleasePool;
    NSWindow *_window;
    NSWindowController *_windowController;

    GlView *_glView;

protected:
    void create(const WindowConfiguration &configuration);
    void destroy();

    void createAutoreleasePool();
public:
    Window();
    Window(const WindowConfiguration &configuration);
    ~Window();

    void start();
};
}

#endif
#endif
