#include "../../os.h"

#ifdef CYBIL_TARGET_OSX
#import <Cocoa/Cocoa.h>
#include <chrono>

namespace cybil {
    class WindowProtocol;
}

@interface GlView : NSOpenGLView
{
    cybil::WindowProtocol *_parentWindow;
    std::chrono::time_point<std::chrono::high_resolution_clock> _lastTime;
}

- (void)swap;
- (void)draw;
- (void)setParentWindow:(cybil::WindowProtocol *)window;

@end
#endif
