#ifndef CYBIL_OS_ANDROID_WINDOW_H
#define CYBIL_OS_ANDROID_WINDOW_H

#include <cybil/os.h>

#ifdef CYBIL_TARGET_ANDROID

#include <cybil/os/window_protocol.h>
#include <cybil/gl.h>
#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

extern int main(int argc, char **argv);

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_MainActivity_nativeInit(JNIEnv* env, jobject obj);
JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_MainActivity_nativeBack(JNIEnv* env, jclass clazz);
JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_MainActivity_setCachePath(JNIEnv* env, jclass clazz, jstring path);
JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilSurfaceView_nativeResume(JNIEnv* env, jclass clazz);
JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilRenderer_nativeResize(JNIEnv* env, jclass clazz, jint w, jint h);
JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilRenderer_nativeDone(JNIEnv* env, jclass clazz);
JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilSurfaceView_nativeTouchBegan(JNIEnv* env, jclass clazz, jint id, jfloat x, jfloat y);
JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilSurfaceView_nativeTouchMoved(JNIEnv* env, jclass clazz, jint id, jfloat x, jfloat y);
JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilSurfaceView_nativeTouchEnded(JNIEnv* env, jclass clazz, jint id, jfloat x, jfloat y);
JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilSurfaceView_nativeTogglePauseResume(JNIEnv* env, jclass clazz);
JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilSurfaceView_nativePause(JNIEnv* env, jclass clazz);
JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilRenderer_nativeRender(JNIEnv* env, jclass clazz);

#ifdef __cplusplus
}
#endif

namespace cybil
{
	class Window : public WindowProtocol
	{

	protected:
		void create(const WindowConfiguration &configuration);
		void destroy();

	public:
		Window();
		Window(const WindowConfiguration &configuration);
		~Window();

		void start();
		void stop();

        void setEventFunction(EventFunction f);
        void setDrawFunction(DrawFunction f);
	};
}

#endif

#endif
