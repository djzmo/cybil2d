#ifndef CYBIL_EVENTS_H
#define CYBIL_EVENTS_H

#include <vector>
#include "types.h"

namespace cybil
{
	enum Key
	{
		A, B, C, D, E, F, G, H, I, J, K, L, M,
		N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
		Number0, Number1, Number2, Number3, Number4,
		Number5, Number6, Number7, Number8, Number9,
		Numpad0, Numpad1, Numpad2, Numpad3, Numpad4,
		Numpad5, Numpad6, Numpad7, Numpad8, Numpad9,
		F1, F2, F3, F4, F5, F6,
		F7, F8, F9, F10, F11, F12,
		UpArrow, LeftArrow, DownArrow, RightArrow, PageUp, PageDown,
		LShift, RShift, LAlt, RAlt, LCtrl, RCtrl,
		Tab, End, Home, Delete, CapsLock, System,
		Insert, Space, BackSpace, Enter, Escape,
		Pause, Menu, Add, Divide, Subtract, Multiply,
		Comma, Period, Slash, BackSlash, SemiColon,
		SingleQuote, LBracket, RBracket, Tilde,
		Dash, Equal, PrintScreen
	};

	enum EventType
	{
		KeyDown, KeyUp,
		MouseButtonDown, MouseButtonUp, MouseMoved,
		MouseWheelScrolled,
		TouchBegan, TouchEnded, TouchMoved,
		WindowClosed, WindowResized,
		TextEntered
	};

	enum MouseButton
	{
		Left, Middle, Right
	};

	class Event
	{
	public:
		EventType type;
		unsigned char charCode;
		Vector mousePosition, touchPosition;
		short mouseWheelDelta;
		MouseButton mouseButton;
		Size windowSize;
		Key key;
		int touchId;
		
		Event(EventType type);
		Event(EventType type, unsigned char charCode);
		Event(EventType type, const Vector &mousePosition);
		Event(EventType type, const Vector &mousePosition, MouseButton mouseButton);
		Event(EventType type, Size windowSize);
		Event(EventType type, Key key);
		Event(EventType type, short mouseWheelDelta);
		Event(EventType type, const Vector &touchPosition, unsigned int touchId);

	};

}

#endif