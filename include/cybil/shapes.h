#ifndef CYBIL_SHAPES_H
#define CYBIL_SHAPES_H

#include "types.h"
#include "drawable.h"
#include "gl.h"

namespace cybil
{
    
class Shape : public Drawable
{
protected:
    Color _fillColor;
    Color _outlineColor;
    float _outlineWidth;
public:
    Shape();
    
    void setFillColor(const Color &color);
    void setOutlineColor(const Color &color);
    void setOutlineWidth(float width);
    const Color &getFillColor() const;
    const Color &getOutlineColor() const;
    float getOutlineWidth();
};
    
    
    
class LineShape : public Shape
{
protected:
    Vector _endPosition;
public:
    LineShape();
    LineShape(const Vector &start, const Vector &end, float width = 1.0f);
    
    void draw(const DrawTarget &target) const;
};
    
    
    
class RectShape : public Shape
{
protected:
    Size _size;
public:
    RectShape();
    RectShape(const Vector &position, const Size &size);
    RectShape(const Vector &position, const Size &size, const Color &fillColor);
    
    void draw(const DrawTarget &target) const;
};
    
    
    
class CircleShape : public Shape
{
protected:
    float _r;
public:
    CircleShape();
    CircleShape(const Vector &position, float r);
    CircleShape(const Vector &position, float r, const Color &fillColor);
    
    void draw(const DrawTarget &target) const;
};
    
    
    
class Shapes
{
public:
    static LineShape line(const Vector &start, const Vector &end, float width = 1.0f);
    static RectShape rect(const Vector &position, const Size &size);
    static RectShape rect(const Vector &position, const Size &size, const Color &fillColor);
    static CircleShape circle(const Vector &position, float r);
    static CircleShape circle(const Vector &position, float r, const Color &fillColor);
};
    
}

#endif