#ifndef CYBIL_TEXT_H
#define CYBIL_TEXT_H

#include <string>
#include "drawable.h"

namespace cybil
{
    
    class Font;
    class DrawTarget;
    class Text : public Drawable
    {
    protected:
        Font *_font;
        std::string _caption;
        Color _color;
        float _size;
		bool _isMultiline;
        
    public:
        Text();
        Text(Font *font);
        virtual ~Text();
        
        void setFont(Font *font);
        Font *getFont();
        void setCaption(const std::string &caption);
        const std::string &getCaption() const;
        void setSize(float size);
        float getSize();
        void setColor(const Color &color);
        const Color &getColor() const;
        
        void draw(const DrawTarget &target) const;
    };
    
}

#endif