#ifndef CYBIL_TYPES_H
#define CYBIL_TYPES_H

namespace cybil
{
    
typedef signed char int8;
typedef unsigned char uint8;
typedef signed short int16;
typedef unsigned short uint16;
typedef signed int int32;
typedef unsigned int uint32;
typedef signed long long int64;
typedef unsigned long long uint64;

struct Vector
{
    Vector() : x(0), y(0) {}
    Vector(float _x, float _y) : x(_x), y(_y) {}
    
    float x, y;
    
    Vector &operator+(const Vector &right);
    Vector &operator-(const Vector &right);
};
    
struct Vector3d : public Vector
{
    Vector3d() : Vector(), z(0) {}
    Vector3d(float _x, float _y, float _z) : Vector(_x, _y), z(_z) {}
    
    float z;
};

struct Size
{
    Size() : width(0), height(0) {}
    Size(float w, float h) : width(w), height(h) {}
    
    float width, height;
};

struct Rect
{
	Rect() {}
	Rect(float l, float t, float w, float h) : left(l), top(t), width(w), height(h) {}

	float left, top, width, height;
};

struct Color
{
    Color() : r(0), g(0), b(0), a(255) {}
    Color(unsigned char _r, unsigned char _g, unsigned char _b) : r(_r), g(_g), b(_b), a(255) {}
    Color(unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a) : r(_r), g(_g), b(_b), a(_a) {}
    
    uint8 r, g, b, a;
    
    static const Color Black;
    static const Color Gray;
    static const Color White;
    static const Color Red;
    static const Color Green;
    static const Color Blue;
    static const Color Orange;
    static const Color Purple;
    static const Color Transparent;
};
    
}

#endif