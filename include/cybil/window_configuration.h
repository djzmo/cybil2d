#ifndef CYBIL_WINDOW_WINDOWCONFIGURATION_H
#define CYBIL_WINDOW_WINDOWCONFIGURATION_H

#include <string>
#include "types.h"

namespace cybil
{
enum WindowStyle
{
    None = 0,
    FullScreen = 1 << 0,
    // Desktop-only styles
    Close = 1 << 1,
    Resize = 1 << 2,
    Minimize = 1 << 3,
    Border = 1 << 4,
    Title = 1 << 5,
    // Full screen styles
    Stretched = 1 << 6,
    FixedWidth = 1 << 7,
    FixedHeight = 1 << 8
};

struct WindowConfiguration
{
    WindowConfiguration();
    WindowConfiguration(const std::string &windowTitle,
                        const Size &surfaceSize,
                        int bitsPerPixel = 32,
                        int frameRate = 60,
                        WindowStyle windowStyle = static_cast<WindowStyle>(Resize | Minimize | Border | Title | Close));
    
    std::string windowTitle;
    Size surfaceSize;
    int bitsPerPixel, frameRate;
    WindowStyle windowStyle;
};
}

#endif
