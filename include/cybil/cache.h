#ifndef CYBIL_CACHE_H
#define CYBIL_CACHE_H

#include <vector>
#include <string>
#include <map>

namespace cybil
{
    class Resource;
    class Cache
    {
        std::map<std::string, Resource *> _resources;
        
        static Cache _instance;
        
    public:
        static Cache *instance();
        
        void addTexture(const char *path);
        void addFont(const char *path);
    };
}

#endif