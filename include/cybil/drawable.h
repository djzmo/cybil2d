#ifndef CYBIL_DRAWABLE_H
#define CYBIL_DRAWABLE_H

#include "draw_target.h"
#include "types.h"

namespace cybil
{

class Drawable
{
protected:
    Vector _origin;
    Vector _position;
    Vector _scale;
    float _rotation;
    
public:
    virtual ~Drawable();
    Drawable();
    
    void setOrigin(const Vector &origin);
    void setPosition(const Vector &position);
    void setScale(const Vector &scale);
    void setRotation(float angle);
    const Vector &getOrigin() const;
    const Vector &getPosition() const;
    const Vector &getScale() const;
    float getRotation();
    
    virtual void draw(const DrawTarget &target) const = 0;
};
    
}

#endif