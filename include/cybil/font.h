#ifndef CYBIL_FONT_H
#define CYBIL_FONT_H

#include "types.h"
#include "external/fontstash.h"

namespace cybil
{
    class Text;
    class Font
    {
        friend class Text;
        
    protected:
        uint32 _id;
        struct sth_stash *_stash;
    public:
        Font();
        Font(const char *path);
        virtual ~Font();
        
        bool createFromFile(const char *path);
        
        const uint32 getId() const;
    };
}

#endif