#ifndef CYBIL_FILE_UTILS_H
#define CYBIL_FILE_UTILS_H

#include <string>

namespace cybil
{
    class FileUtils
    {
    protected:
    	static FileUtils *_instance;
        std::string _cacheDir;

		FileUtils();

    public:
		~FileUtils();

    	static FileUtils *getInstance();
        std::string getRealPath(const char *path);
        std::string getCacheDir();
        void setCacheDir(const std::string &path);
    };
}

#endif