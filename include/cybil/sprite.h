#ifndef CYBIL_SPRITE_H
#define CYBIL_SPRITE_H

#include "drawable.h"

namespace cybil
{
    
class Texture;
class DrawTarget;
class Sprite : public Drawable
{
protected:
    Texture *_texture;
	Rect _textureRect;
    
public:
    Sprite();
    Sprite(Texture *texture);
	Sprite(Texture *texture, const Rect &textureRect);
    virtual ~Sprite();
    
    void setTexture(Texture *texture);
    Texture *getTexture();
	void setTextureRect(const Rect &textureRect);
	const Rect &getTextureRect() const;
    
    void draw(const DrawTarget &target) const;
};
    
}

#endif