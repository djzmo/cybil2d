#ifndef CYBIL_PLATFORM_H
#define CYBIL_PLATFORM_H

#ifdef _WIN32
#   define CYBIL_TARGET_WIN32
#   define CYBIL_CPP11_SUPPORT
#elif defined(__APPLE__)
#   include "TargetConditionals.h"
#   if defined(TARGET_OS_MAC)
#       define CYBIL_TARGET_OSX
#   else
#       define CYBIL_TARGET_IOS
#   endif
#elif defined(NETFX_CORE)
#   define CYBIL_TARGET_WINRT
#elif defined(WINDOWS_PHONE)
#   define CYBIL_TARGET_WP8
#elif defined(__QNX__)
#   define CYBIL_TARGET_QNX
#   define nullptr NULL
#elif defined(ANDROID) || defined(__ANDROID__)
#   define CYBIL_TARGET_ANDROID
#   define CYBIL_CPP11_SUPPORT
#elif defined(__linux)
#   define CYBIL_TARGET_LINUX
#else
#   define CYBIL_TARGET_UNKNOWN
#endif

#endif
