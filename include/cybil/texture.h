#ifndef CYBIL_TEXTURE_H
#define CYBIL_TEXTURE_H

#include "types.h"

namespace cybil
{
class Texture
{
protected:
    uint32 _id;
    Size _size;
public:
    Texture();
    Texture(const char *path);
    virtual ~Texture();
    
    void createFromFile(const char *path);
    
    const uint32 getId() const;
    const Size &getSize() const;
};
}

#endif