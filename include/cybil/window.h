#ifndef CYBIL_WINDOW_H
#define CYBIL_WINDOW_H

#if defined(CYBIL_TARGET_OSX)
#   include "os/osx/window.h"
#elif defined(CYBIL_TARGET_WIN32)
#   include "os/win32/window.h"
#elif defined(CYBIL_TARGET_ANDROID)
#   include "os/android/window.h"
#elif defined(CYBIL_TARGET_QNX)
#   include "os/qnx/window.h"
#endif

#endif