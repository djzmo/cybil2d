#ifndef CYBIL_DRAWTARGET_H
#define CYBIL_DRAWTARGET_H

namespace cybil
{

struct Color;
struct Size;
class DrawTarget;
class Drawable;

typedef void(*DrawFunction)(const DrawTarget &target, double delta);

class DrawTarget
{
protected:
    virtual ~DrawTarget();
public:
    DrawTarget();
    
    virtual void setDrawFunction(DrawFunction f);
    const DrawFunction &getDrawFunction() const;
	virtual const Size &getSurfaceSize() const = 0;

	void draw(const Drawable &drawable) const;

    DrawFunction _drawFunction;
};
    
}

#endif
