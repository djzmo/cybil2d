#include <cybil/shapes.h>
#include <cybil/draw_target.h>
#include <cybil/gl.h>
#include <math.h>

#define toRadians(angleDegrees) (angleDegrees * M_PI / 180.0)

namespace cybil
{
    
    Shape::Shape() :
    Drawable(),
    _fillColor(Color::White),
    _outlineColor(Color::White),
    _outlineWidth(1)
    {
    }
    
    void Shape::setFillColor(const cybil::Color &color)
    {
        _fillColor = color;
    }
    
    void Shape::setOutlineColor(const cybil::Color &color)
    {
        _outlineColor = color;
    }
    
    void Shape::setOutlineWidth(float width)
    {
        _outlineWidth = width;
    }
    
    const Color &Shape::getFillColor() const
    {
        return _fillColor;
    }
    
    const Color &Shape::getOutlineColor() const
    {
        return _outlineColor;
    }
    
    float Shape::getOutlineWidth()
    {
        return _outlineWidth;
    }
    
    LineShape::LineShape() :
    Shape(),
    _endPosition(Vector(0, 0))
    {
    }
    
    LineShape::LineShape(const Vector &start, const Vector &end, float width) :
    Shape(),
    _endPosition(end)
    {
        setPosition(start);
        setOutlineWidth(width);
    }
    
    void LineShape::draw(const DrawTarget &target) const
    {
        glColor3f(_outlineColor.r / 255.f, _outlineColor.g / 255.f, _outlineColor.b / 255.f);
        glLineWidth(_outlineWidth);

		GLfloat points[] = { _position.x, _position.y, _endPosition.x, _endPosition.y };
        glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(2, GL_FLOAT, 0, points);
		glDrawArrays(GL_LINES, 0, 2);
        glDisableClientState(GL_VERTEX_ARRAY);

        glLineWidth(1);
    }
    
    RectShape::RectShape() :
    Shape(),
    _size(Size(0, 0))
    {
    }
    
    RectShape::RectShape(const Vector &position, const Size &size) :
    Shape(),
    _size(size)
    {
        setPosition(position);
        setOutlineWidth(0);
    }
    
    RectShape::RectShape(const Vector &position, const Size &size, const Color &fillColor) :
    Shape(),
    _size(size)
    {
        setPosition(position);
        setOutlineWidth(0);
		setFillColor(fillColor);
    }
    
    void RectShape::draw(const DrawTarget &target) const
    {
        glColor3f(_fillColor.r / 255.f, _fillColor.g / 255.f, _fillColor.b / 255.f);
        glLineWidth(_outlineWidth);
        
        GLfloat q3[] = {
            _position.x, _position.y, 0,
            _position.x + _size.width, _position.y, 0,
            _position.x + _size.width, _position.y + _size.height, 0,
            _position.x, _position.y + _size.height, 0
        };
        
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, q3);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glDisableClientState(GL_VERTEX_ARRAY);
        
        glLineWidth(1);
    }
    
    CircleShape::CircleShape() :
    Shape(),
    _r(0)
    {
    }
    
    CircleShape::CircleShape(const Vector &position, float r) :
    Shape(),
    _r(r)
    {
        setPosition(position);
        setOutlineWidth(0);
    }
    
    CircleShape::CircleShape(const Vector &position, float r, const Color &fillColor) :
    Shape(),
    _r(r)
    {
        setPosition(position);
        setOutlineWidth(0);
		setFillColor(fillColor);
    }
    
    void CircleShape::draw(const DrawTarget &target) const
    {
        glColor3f(_fillColor.r / 255.f, _fillColor.g / 255.f, _fillColor.b / 255.f);
        
        int sides = 32;
		int vCount = 0;
        float *vertices = new float[(sides + 1) * 2];
        for (int i = 0; i < 360.0f; i += (360.0f / sides))
		{
            vertices[vCount++] = _position.x + cos(toRadians(i)) * _r;
			vertices[vCount++] = _position.y + sin(toRadians(i)) * _r;
        }
        
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer (2, GL_FLOAT , 0, vertices);
        glDrawArrays (GL_TRIANGLE_FAN, 0, sides);
        glDisableClientState(GL_VERTEX_ARRAY);

		delete[] vertices;
        
        glColor3f(1, 1, 1);
    }
    
    
    
    LineShape Shapes::line(const Vector &start, const Vector &end, float width)
    {
        return LineShape(start, end, width);
    }
    
    RectShape Shapes::rect(const Vector &position, const Size &size)
    {
        return RectShape(position, size);
    }
    
    RectShape Shapes::rect(const Vector &position, const Size &size, const Color &fillColor)
    {
        return RectShape(position, size, fillColor);
    }
    
    CircleShape Shapes::circle(const Vector &position, float r)
    {
        return CircleShape(position, r);
    }
    
    CircleShape Shapes::circle(const Vector &position, float r, const Color &fillColor)
    {
        return CircleShape(position, r, fillColor);
    }
    
}