#include <cybil/text.h>
#include <cybil/draw_target.h>
#include <cybil/gl.h>
#include <cybil/font.h>
#include <sstream>
#include <iostream>

namespace cybil
{
    Text::Text() :
    Drawable(),
    _font(nullptr),
    _caption(""),
    _size(12),
    _color(Color(255, 255, 255, 255))
    {
    }
    
    Text::Text(Font *font) :
    Drawable(),
    _font(font),
    _caption(""),
    _size(12),
    _color(Color(255, 255, 255, 255))
    {
    }
    
    Text::~Text()
    {
    }
    
    void Text::setFont(Font *font)
    {
        _font = font;
    }
    
    Font *Text::getFont()
    {
        return _font;
    }
    
    void Text::setCaption(const std::string &caption)
    {
        _caption = caption;
		_isMultiline = _caption.find("\n") != std::string::npos;
    }
    
    const std::string &Text::getCaption() const
    {
        return _caption;
    }
    
    void Text::setSize(float size)
    {
        _size = size;
    }
    
    float Text::getSize()
    {
        return _size;
    }
    
    void Text::setColor(const cybil::Color &color)
    {
        _color = color;
    }
    
    const Color &Text::getColor() const
    {
        return _color;
    }
    
    void Text::draw(const DrawTarget &target) const
    {
        if(_font == nullptr && _font->_stash != nullptr)
            return;
        
        glColor3f(_color.r / 255.f, _color.g / 255.f, _color.b / 255.f);
        
        sth_begin_draw(_font->_stash);
		if(_isMultiline)
		{
			std::stringstream ss(_caption);
			std::string line;
			int lineNum = 0;
			while(std::getline(ss, line, '\n'))
			{
				sth_draw_text(_font->_stash, _font->getId(), _size, _position.x, _position.y + _size * lineNum - _size, line.c_str(), nullptr);
				lineNum++;
			}
		}
		else sth_draw_text(_font->_stash, _font->getId(), _size, _position.x, _position.y, _caption.c_str(), nullptr);
        sth_end_draw(_font->_stash);
        
        glColor3f(1, 1, 1);
    }
}