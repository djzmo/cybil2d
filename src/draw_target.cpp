#include <cybil/draw_target.h>
#include <cybil/drawable.h>
#include <cybil/types.h>
#include <cybil/gl.h>

namespace cybil
{
    
DrawTarget::DrawTarget() :
_drawFunction(nullptr)
{
}

DrawTarget::~DrawTarget()
{
}

void DrawTarget::setDrawFunction(DrawFunction f)
{
    _drawFunction = f;
}

const DrawFunction &DrawTarget::getDrawFunction() const
{
    return _drawFunction;
}

void DrawTarget::draw(const Drawable &drawable) const
{
	drawable.draw(*this);
}

}