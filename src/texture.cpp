#include <cybil/texture.h>
#include <cybil/gl.h>
#include <cybil/file_utils.h>

#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#endif

#include <cybil/external/stb_image.h>

namespace cybil
{

Texture::Texture() :
_size(0, 0),
_id(0)
{
}

Texture::Texture(const char *path) :
_size(0, 0),
_id(0)
{
    createFromFile(path);
}

Texture::~Texture()
{
}

void Texture::createFromFile(const char *path)
{
    int32 width, height, comp;
    uint8 *image = stbi_load(FileUtils::getInstance()->getRealPath(path).c_str(), &width, &height, &comp, STBI_rgb_alpha);
    
    if(image)
    {
        glGenTextures(1, &_id);
        glBindTexture(GL_TEXTURE_2D, _id);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        
        glTexImage2D(GL_TEXTURE_2D, 0, comp == 4 ? GL_RGBA : GL_RGB, width, height, 0, comp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, image);
        
        glBindTexture(GL_TEXTURE_2D, 0);
        
        _size = Size(width, height);
        
        stbi_image_free(image);
    }
}

const Size &Texture::getSize() const
{
    return _size;
}

const uint32 Texture::getId() const
{
    return _id;
}
    
}