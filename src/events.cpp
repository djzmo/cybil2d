#include <cybil/events.h>

namespace cybil
{
    
    Event::Event(EventType type)
    {
        this->type = type;
    }
    
    Event::Event(EventType type, unsigned char charCode)
	{
		this->type = type;
        this->charCode = charCode;
    }
    
    Event::Event(EventType type, Key key)
    {
		this->type = type;
        this->key = key;
    }
    
    Event::Event(EventType type, short mouseWheelDelta)
    {
		this->type = type;
        this->mouseWheelDelta = mouseWheelDelta;
    }
    
    Event::Event(EventType type, const Vector &mousePosition)
    {
		this->type = type;
        this->mousePosition = mousePosition;
    }
    
    Event::Event(EventType type, const Vector &mousePosition, MouseButton mouseButton)
    {
		this->type = type;
        this->mousePosition = mousePosition;
        this->mouseButton = mouseButton;
    }
    
    Event::Event(EventType type, const Vector &touchPosition, unsigned int touchId)
    {
		this->type = type;
        this->touchPosition = touchPosition;
        this->touchId = touchId;
    }
    
    Event::Event(EventType type, Size windowSize)
    {
		this->type = type;
        this->windowSize = windowSize;
    }
    
}