#include <cybil/file_utils.h>
#include <cybil/os.h>

namespace cybil
{
	FileUtils *FileUtils::_instance = nullptr;

	FileUtils::FileUtils()
	{
	}

	FileUtils::~FileUtils()
	{
		_instance = nullptr;
	}

	FileUtils *FileUtils::getInstance()
	{
		if(_instance == nullptr)
			_instance = new FileUtils;

		return _instance;
	}

    std::string FileUtils::getRealPath(const char *path)
    {
        return _cacheDir + path;
    }

	std::string FileUtils::getCacheDir()
	{
		return _cacheDir;
	}

	void FileUtils::setCacheDir(const std::string &path)
	{
		_cacheDir = path;
	}

}