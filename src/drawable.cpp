#include <cybil/drawable.h>

namespace cybil
{

    Drawable::Drawable()
    {
    }
    
    Drawable::~Drawable()
    {
    }
    
    void Drawable::setOrigin(const Vector &origin)
    {
        _origin = origin;
    }
    
    void Drawable::setPosition(const Vector &position)
    {
        _position = position;
    }
    
    void Drawable::setScale(const Vector &scale)
    {
        _scale = scale;
    }
    
    void Drawable::setRotation(float rotation)
    {
        _rotation = rotation;
    }
    
    const Vector &Drawable::getOrigin() const
    {
        return _origin;
    }
    
    const Vector &Drawable::getPosition() const
    {
        return _position;
    }
    
    const Vector &Drawable::getScale() const
    {
        return _scale;
    }
    
    float Drawable::getRotation()
    {
        return _rotation;
    }
}