#include <cybil/window_configuration.h>

namespace cybil
{

WindowConfiguration::WindowConfiguration() :
    windowTitle("Cybil2D Application"),
    surfaceSize(0, 0),
    bitsPerPixel(16),
    frameRate(30),
    windowStyle()
{
}

WindowConfiguration::WindowConfiguration(const std::string &wt, const Size &ss, int bpp, int fps, WindowStyle ws) :
    windowTitle(wt),
    surfaceSize(ss),
    bitsPerPixel(bpp),
    frameRate(fps),
    windowStyle(ws)
{
}

}
