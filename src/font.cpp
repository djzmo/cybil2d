#include <cybil/font.h>
#include <cybil/gl.h>
#include <cybil/file_utils.h>

namespace cybil
{
    
    Font::Font() :
    _id(0),
    _stash(nullptr)
    {
    }
    
    Font::Font(const char *path) :
    _id(0),
    _stash(nullptr)
    {
        createFromFile(path);
    }
    
    Font::~Font()
    {
        if(_stash != nullptr)
            sth_delete(_stash);
    }
    
    bool Font::createFromFile(const char *path)
    {
        if(_stash == nullptr)
            _stash = sth_create(512, 512);
        
        _id = sth_add_font(_stash, FileUtils::getInstance()->getRealPath(path).c_str());
        
        return _id != 0;
    }
    
    const uint32 Font::getId() const
    {
        return _id;
    }
    
}