#include <cybil/sprite.h>
#include <cybil/draw_target.h>
#include <cybil/gl.h>
#include <cybil/texture.h>

namespace cybil
{
    
Sprite::Sprite() :
Drawable(),
_texture(nullptr)
{
}

Sprite::Sprite(Texture *texture) :
Drawable(),
_texture(texture)
{
    if(texture != nullptr)
        _textureRect = Rect(0, 0, texture->getSize().width, texture->getSize().height);
}

Sprite::Sprite(Texture *texture, const Rect &textureRect) :
Drawable(),
_texture(texture),
_textureRect(textureRect)
{
}

Sprite::~Sprite()
{
}

void Sprite::setTexture(Texture *texture)
{
    _texture = texture;

	if(texture != nullptr)
	{
		_textureRect.width = texture->getSize().width;
		_textureRect.height = texture->getSize().height;
	}
}

Texture *Sprite::getTexture()
{
    return _texture;
}

void Sprite::setTextureRect(const cybil::Rect &textureRect)
{
    _textureRect = textureRect;
}

const Rect &Sprite::getTextureRect() const
{
    return _textureRect;
}

void Sprite::draw(const DrawTarget &target) const
{
    if(_texture == nullptr)
        return;

    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glBindTexture(GL_TEXTURE_2D, _texture->getId());

    const Size &textureSize = _texture->getSize();

    GLfloat vertex[] = { _position.x, _position.y,
                            _position.x + _textureRect.width, _position.y,
                            _position.x, _position.y + _textureRect.height,
                            _position.x + _textureRect.width, _position.y + _textureRect.height };
    GLfloat texCoord[] = {	_textureRect.left / textureSize.width, _textureRect.top / textureSize.height,
							(_textureRect.left + _textureRect.width) / textureSize.width, _textureRect.top / textureSize.height,
							_textureRect.left / textureSize.width, (_textureRect.top + _textureRect.height) / textureSize.height,
							(_textureRect.left + _textureRect.width) / textureSize.width, (_textureRect.top + _textureRect.height) / textureSize.height };

    glTexCoordPointer(2, GL_FLOAT, 0, texCoord);
    glVertexPointer(2, GL_FLOAT, 0, vertex);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_2D);
}

}