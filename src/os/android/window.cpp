#include <cybil/os.h>

#ifdef CYBIL_TARGET_ANDROID

#include <cybil/os/android/window.h>
#include <cybil/events.h>
#include <jni.h>
#include <sys/time.h>
#include <time.h>
#include <android/log.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <stdint.h>
#include <cybil/gl.h>
#include <cybil/file_utils.h>

#define LOG_TAG "libcybilapp"
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__))

static bool initiated = false;
static int windowWidth = 0;
static int windowHeight = 0;
static cybil::Window *self = nullptr;
static cybil::WindowConfiguration wc;
static cybil::DrawFunction df = nullptr;
static cybil::EventFunction ef = nullptr;

static long _getTime()
{
    struct timeval  now;

    gettimeofday(&now, NULL);
    return (long)(now.tv_sec*1000 + now.tv_usec/1000);
}

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_MainActivity_setCachePath(JNIEnv* env, jclass clazz, jstring path)
{
    cybil::FileUtils::getInstance()->setCacheDir(env->GetStringUTFChars(path, NULL));
}

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_MainActivity_nativeInit(JNIEnv* env, jobject obj)
{
}

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_MainActivity_nativeBack(JNIEnv* env, jclass clazz)
{
    if(ef != nullptr)
        ef(cybil::Event(cybil::EventType::KeyUp, cybil::Key::Escape));
}

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilRenderer_nativeResize(JNIEnv* env, jclass clazz, jint width, jint height)
{
    windowWidth = width;
    windowHeight = height;

    if(ef != nullptr)
        ef(cybil::Event(cybil::EventType::WindowResized, cybil::Size(width, height)));
}

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilRenderer_nativeDone(JNIEnv* env, jclass clazz)
{
}

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilSurfaceView_nativeTouchBegan(JNIEnv* env, jclass clazz, jint id, jfloat x, jfloat y)
{
    if(ef != nullptr)
        ef(cybil::Event(cybil::EventType::TouchBegan, cybil::Vector(x * wc.surfaceSize.width / (float)windowWidth, y * wc.surfaceSize.height / (float)windowHeight), id));
}

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilSurfaceView_nativeTouchMoved(JNIEnv* env, jclass clazz, jint id, jfloat x, jfloat y)
{
    if(ef != nullptr)
        ef(cybil::Event(cybil::EventType::TouchMoved, cybil::Vector(x * wc.surfaceSize.width / (float)windowWidth, y * wc.surfaceSize.height / (float)windowHeight), id));
}

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilSurfaceView_nativeTouchEnded(JNIEnv* env, jclass clazz, jint id, jfloat x, jfloat y)
{
    if(ef != nullptr)
        ef(cybil::Event(cybil::EventType::TouchEnded, cybil::Vector(x * wc.surfaceSize.width / (float)windowWidth, y * wc.surfaceSize.height / (float)windowHeight), id));
}

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilSurfaceView_nativeTogglePauseResume(JNIEnv* env, jclass clazz)
{
}

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilSurfaceView_nativePause(JNIEnv* env, jclass clazz)
{
}

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilSurfaceView_nativeResume(JNIEnv* env, jclass clazz)
{
}

JNIEXPORT void JNICALL Java_com_djzmo_skripsi_cybilapp_CybilRenderer_nativeRender(JNIEnv* env, jclass clazz)
{
    if(!initiated)
    {
        main(0, nullptr);
        initiated = true;
    }

	glClearColorx((GLfixed) 0, (GLfixed) 0, (GLfixed) 0, 0x10000);

    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    glViewport(0, 0, windowWidth, windowHeight);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrthof(0, wc.surfaceSize.width, wc.surfaceSize.height, 0, -1, 1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glDisable(GL_DEPTH_TEST);
    glColor4ub(255, 255, 255, 255);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    if(df != nullptr && self != nullptr)
        df(*self, _getTime());
}

namespace cybil
{

	Window::Window() : WindowProtocol()
	{
		_windowConfig.windowTitle = "Cybil";
		_windowConfig.surfaceSize = Size(800, 600);
		_windowConfig.bitsPerPixel = 32;
		_windowConfig.windowStyle = static_cast<WindowStyle>(Resize | Minimize | Close | Title);

		create(_windowConfig);
	}

	Window::Window(const WindowConfiguration &configuration) : WindowProtocol(configuration)
	{
		create(configuration);
	}

	Window::~Window()
	{
		destroy();
	}

	void Window::create(const cybil::WindowConfiguration &configuration)
	{
	    self = this;
	    wc = configuration;
	}

	void Window::start()
	{
	}

	void Window::stop()
	{
	}

	void Window::destroy()
	{
	}

	void Window::setEventFunction(EventFunction f)
	{
	    WindowProtocol::setEventFunction(f);
	    ef = f;
	}

	void Window::setDrawFunction(DrawFunction f)
	{
	    DrawTarget::setDrawFunction(f);
	    df = f;
	}

}
#endif
