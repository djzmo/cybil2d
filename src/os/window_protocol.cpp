#include <cybil/drawable.h>
#include <cybil/os/window_protocol.h>
#include <cybil/window_configuration.h>

namespace cybil
{

WindowProtocol::WindowProtocol() :
    _eventFunction(NULL),
    _isOpen(false),
    _isAcceptingMouseMovedEvents(false),
	_isEmulatingSingleTouch(true)
{
}

WindowProtocol::WindowProtocol(const WindowConfiguration &configuration) :
    _eventFunction(NULL),
    _windowConfig(configuration),
    _isOpen(false),
    _isAcceptingMouseMovedEvents(false),
	_isEmulatingSingleTouch(true)
{
}

WindowProtocol::~WindowProtocol()
{
}

const WindowConfiguration &WindowProtocol::getConfiguration() const
{
    return _windowConfig;
}

void WindowProtocol::setEventFunction(EventFunction f)
{
    _eventFunction = f;
}

bool WindowProtocol::isOpen()
{
    return _isOpen;
}

const EventFunction &WindowProtocol::getEventFunction() const
{
    return _eventFunction;
}
    
void WindowProtocol::setAcceptsMouseMovedEvents(bool flag)
{
    _isAcceptingMouseMovedEvents = flag;
}

bool WindowProtocol::isAcceptingMouseMovedEvents()
{
    return _isAcceptingMouseMovedEvents;
}

void WindowProtocol::setEmulateSingleTouch(bool flag)
{
	_isEmulatingSingleTouch = flag;
}

bool WindowProtocol::isEmulatingSingleTouch()
{
	return _isEmulatingSingleTouch;
}

void WindowProtocol::setFrameRate(int frameRate)
{
    _windowConfig.frameRate = frameRate;
}

const Size &WindowProtocol::getSurfaceSize() const
{
	return _windowConfig.surfaceSize;
}

}
