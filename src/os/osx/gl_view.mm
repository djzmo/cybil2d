#include <cybil/os.h>

#ifdef CYBIL_TARGET_OSX
#import <cybil/os/osx/gl_view.h>
#include <cybil/gl.h>
#include <cybil/os/window_protocol.h>
#include <cybil/events.h>

using namespace std::chrono;
using namespace cybil;

@implementation GlView

- (void)prepareOpenGL
{
    [super prepareOpenGL];
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    
    glDisable(GL_DEPTH_TEST);
    
    GLint swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
}

- (void)reshape
{
    [super reshape];
    [[self openGLContext] update];
    
    CGSize windowSize = [self bounds].size;
    double windowAspect = windowSize.width / windowSize.height;
    
    glViewport(0, 0, windowSize.width, windowSize.height);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    if(_parentWindow != nullptr)
    {
        WindowConfiguration conf = _parentWindow->getConfiguration();
        cybil::Size targetSize = conf.surfaceSize;
        double targetAspect = targetSize.width / targetSize.height;
        
        if(conf.windowStyle & WindowStyle::KeepAspect)
        {
            double wOff = 0;
            double hOff = 0;

            double left = wOff;
            double right = targetSize.width + wOff;
            double bottom = targetSize.height + hOff;
            double top = hOff;
            
            glOrtho(left, right, bottom, top, -1, 1);
        }
        else
            glOrtho(0, targetSize.width, targetSize.height, 0, -1, 1);
    }
    else
    {
        glOrtho(0, windowSize.width, windowSize.height, 0, -1, 1);
    }
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

- (id)initWithFrame:(NSRect)frameRect
{
    _parentWindow = nullptr;
    _timer = nullptr;
    
    GLuint attributes[] = {
        NSOpenGLPFANoRecovery,
        NSOpenGLPFAWindow,
        NSOpenGLPFAAccelerated,
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFADepthSize, 32,
        NSOpenGLPFAColorSize, 24,
        NSOpenGLPFAAlphaSize, 8,
        NSOpenGLPFAStencilSize, 8,
        0
    };
    
    NSOpenGLPixelFormat *pfd = [[NSOpenGLPixelFormat alloc] initWithAttributes:(NSOpenGLPixelFormatAttribute *)attributes];
    self = [super initWithFrame:frameRect pixelFormat:pfd];
    
    [[self openGLContext] makeCurrentContext];
    
    _lastTime = high_resolution_clock::now();
    
    [self setAcceptsTouchEvents:true];
    
    return self;
}

- (void)start
{
    if(_timer != nullptr)
    {
        [_timer invalidate];
        _timer = nullptr;
    }
    
    double fps = 60.0;
    
    if(_parentWindow != nullptr)
        fps = (double) _parentWindow->getConfiguration().frameRate;
    
    _timer = [NSTimer timerWithTimeInterval:1.0/fps
                                       target:self
                                     selector:@selector(draw)
                                     userInfo:nil
                                      repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:_timer
                                 forMode:NSDefaultRunLoopMode];
    [[NSRunLoop currentRunLoop] addTimer:_timer
                                 forMode:NSEventTrackingRunLoopMode];
    
    _lastTime = high_resolution_clock::now();
}

- (void)draw
{
    glClear(GL_COLOR_BUFFER_BIT);
    
    if(_parentWindow != nullptr)
    {
        const cybil::DrawFunction &f = _parentWindow->getDrawFunction();
        if(f != nullptr)
        {
            auto now = high_resolution_clock::now();
            f(*_parentWindow, duration_cast<milliseconds>(now - _lastTime).count());
            _lastTime = now;
        }
    }
    
    [self swap];
}

- (void)swap
{
    glFlush();
    [self setNeedsDisplay:YES];
    [[self openGLContext] flushBuffer];
}

- (void)setParentWindow:(cybil::WindowProtocol *)window
{
    _parentWindow = window;
}

- (void)mouseDown:(NSEvent *)theEvent
{
    if(_parentWindow != nullptr)
    {
        const cybil::EventFunction &f = _parentWindow->getEventFunction();
        if(f != nullptr)
        {
            NSPoint point = [self convertPoint:[theEvent locationInWindow] fromView:nil];
            f(Event(EventType::MouseButtonDown, Vector(point.x, point.y), MouseButton::Left));
        }
    }
}

- (void)rightMouseDown:(NSEvent *)theEvent
{
    if(_parentWindow != nullptr)
    {
        const cybil::EventFunction &f = _parentWindow->getEventFunction();
        if(f != nullptr)
        {
            NSPoint point = [self convertPoint:[theEvent locationInWindow] fromView:nil];
            f(Event(EventType::MouseButtonDown, Vector(point.x, point.y), MouseButton::Right));
        }
    }
}

- (void)otherMouseDown:(NSEvent *)theEvent
{
    if(_parentWindow != nullptr)
    {
        const cybil::EventFunction &f = _parentWindow->getEventFunction();
        if(f != nullptr)
        {
            NSPoint point = [self convertPoint:[theEvent locationInWindow] fromView:nil];
            f(Event(EventType::MouseButtonDown, Vector(point.x, point.y), MouseButton::Middle));
        }
    }
}

- (void)mouseUp:(NSEvent *)theEvent
{
    if(_parentWindow != nullptr)
    {
        const cybil::EventFunction &f = _parentWindow->getEventFunction();
        if(f != nullptr)
        {
            NSPoint point = [self convertPoint:[theEvent locationInWindow] fromView:nil];
            f(Event(EventType::MouseButtonUp, Vector(point.x, point.y), MouseButton::Left));
        }
    }
}

- (void)rightMouseUp:(NSEvent *)theEvent
{
    if(_parentWindow != nullptr)
    {
        const cybil::EventFunction &f = _parentWindow->getEventFunction();
        if(f != nullptr)
        {
            NSPoint point = [self convertPoint:[theEvent locationInWindow] fromView:nil];
            f(Event(EventType::MouseButtonUp, Vector(point.x, point.y), MouseButton::Right));
        }
    }
}

- (void)otherMouseUp:(NSEvent *)theEvent
{
    if(_parentWindow != nullptr)
    {
        const cybil::EventFunction &f = _parentWindow->getEventFunction();
        if(f != nullptr)
        {
            NSPoint point = [self convertPoint:[theEvent locationInWindow] fromView:nil];
            f(Event(EventType::MouseButtonUp, Vector(point.x, point.y), MouseButton::Middle));
        }
    }
}

- (void)mouseMoved:(NSEvent *)theEvent
{
    if(_parentWindow != nullptr)
    {
        const cybil::EventFunction &f = _parentWindow->getEventFunction();
        if(_parentWindow->isAcceptingMouseMovedEvents() && f != nullptr)
        {
            NSPoint point = [self convertPoint:[theEvent locationInWindow] fromView:nil];
            f(Event(EventType::MouseMoved, Vector(point.x, [self bounds].size.height - point.y)));
        }
    }
}

- (void)scrollWheel:(NSEvent *)theEvent
{
    if(_parentWindow != nullptr)
    {
        const cybil::EventFunction &f = _parentWindow->getEventFunction();
        if(f != nullptr)
        {
            f(Event(EventType::MouseWheelScrolled, Vector3d([theEvent deltaX], [theEvent deltaY], [theEvent deltaZ])));
        }
    }
}
/*
- (void)touchesBeganWithEvent:(NSEvent *)theEvent
{
    if(_parentWindow != nullptr)
    {
        const cybil::EventFunction &f = _parentWindow->getEventFunction();
        if(f != nullptr)
        {
            NSSet *touches = [theEvent touchesMatchingPhase:NSTouchPhaseTouching inView:self];
            Event e(EventType::TouchBegan);
            
            NSArray *touchesArr = [touches allObjects];
            
            for(int i = 0; i < touches.count; i++)
            {
                NSPoint pt = [self convertPointFromBase:[theEvent locationInWindow]];
                TouchInfo info;
                info.position = Vector(pt.x, pt.y);
                info.id = [touchesArr objectAtIndex:i].identity;
                
                e.touches.push_back(info);
            }
            
            f(e);
        }
    }
}
*/
@end
#endif