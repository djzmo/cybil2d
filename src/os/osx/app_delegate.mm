#include <cybil/os.h>

#ifdef CYBIL_TARGET_OSX
#include <cybil/os/osx/app_delegate.h>

@implementation AppDelegate : NSObject

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

- (void)windowDidBecomeMain:(NSNotification *)notification
{
}

- (NSWindow *)window
{
    return self.window;
}

- (void)setWindow:(NSWindow *)w
{
    window = w;
}

@end
#endif