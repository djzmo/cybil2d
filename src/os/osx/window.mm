#include <cybil/os.h>

#ifdef CYBIL_TARGET_OSX
#include <cybil/drawable.h>
#include <cybil/os/osx/window.h>
#import <cybil/os/osx/gl_view.h>

namespace cybil
{
    
Window::Window() : WindowProtocol()
{
    _windowConfig.windowTitle = "Cybil";
    _windowConfig.surfaceSize = Size(800, 600);
    _windowConfig.bitsPerPixel = 32;
    _windowConfig.windowStyle = static_cast<WindowStyle>(Resize | Minimize | Close | Title);
    
    createAutoreleasePool();
    create(_windowConfig);
}

Window::Window(const WindowConfiguration &configuration) : WindowProtocol(configuration)
{
    createAutoreleasePool();
    create(configuration);
}

Window::~Window()
{
    destroy();
}

void Window::createAutoreleasePool()
{
    _autoreleasePool = [[NSAutoreleasePool alloc] init];
    [NSApplication sharedApplication];
}

void Window::create(const cybil::WindowConfiguration &configuration)
{
    _windowConfig = configuration;
    
    NSUInteger style;
    NSRect contentRect;
    bool fullScreen = configuration.windowStyle & FullScreen;
    
    if(!fullScreen)
    {
        if(configuration.windowStyle & Close)
            style |= NSClosableWindowMask;
        if(configuration.windowStyle & Resize)
            style |= NSResizableWindowMask;
        if(configuration.windowStyle & Title)
            style |= NSTitledWindowMask;
        if(!(configuration.windowStyle & Border))
            style |= NSBorderlessWindowMask;
        if(configuration.windowStyle & Minimize)
            style |= NSMiniaturizableWindowMask;
        
        contentRect = NSMakeRect(100, 100, configuration.surfaceSize.width, configuration.surfaceSize.height);
    }
    else
    {
        style |= NSBackingStoreBuffered | NSFullScreenWindowMask;
        
        contentRect = [[NSScreen mainScreen] frame];
    }
    
    _window = [[NSWindow alloc] initWithContentRect:contentRect styleMask:style backing:NSBackingStoreBuffered defer:NO];
    
    [_window setTitle:[NSString stringWithCString: configuration.windowTitle.c_str() encoding:[NSString defaultCStringEncoding]]];
    [_window center];
    
    if(configuration.windowStyle & Resize)
        [_window setCollectionBehavior:NSWindowCollectionBehaviorFullScreenPrimary];
    
    if(fullScreen)
    {
        [_window setOpaque:YES];
        [_window setLevel:NSFloatingWindowLevel];
        [_window setHidesOnDeactivate:YES];
        [NSApp setPresentationOptions:NSApplicationPresentationHideMenuBar | NSApplicationPresentationHideDock];
    }
    
    _windowController = [[NSWindowController alloc] initWithWindow: _window];
    [_windowController autorelease];

    _glView = [[GlView alloc] initWithFrame:[[_window contentView] frame]];
    [_glView setParentWindow: this];
    
    [_window setContentView:_glView];
    [_window makeFirstResponder:_glView];
    [_window setAcceptsMouseMovedEvents:true];
    
    [_window orderFrontRegardless];
    
    AppDelegate *appDelegate = [[AppDelegate alloc] init];
    [appDelegate setWindow:_window];
    [NSApp setDelegate:appDelegate];
}
    
void Window::setFrameRate(int frameRate)
{
    _windowConfig.frameRate = frameRate;
    [_glView start];
}
    
void Window::setAcceptsMouseMovedEvents(bool flag)
{
    [_window setAcceptsMouseMovedEvents:flag];
}
    
void Window::start()
{
    [_glView start];
    [NSApp run];
    
    destroy();
}

void Window::destroy()
{
    [_autoreleasePool drain];
}
    
}
#endif