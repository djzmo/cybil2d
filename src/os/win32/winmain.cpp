#include <cybil/os.h>

#ifdef CYBIL_TARGET_WIN32

#include <Windows.h>
#include <cstdio>

extern "C"
{
	int main(int argc, char **argv);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	return main(__argc, __argv);
}

#endif
