#include <cybil/os.h>

#ifdef CYBIL_TARGET_WIN32

#include <cybil/os/win32/window.h>
#include <cybil/file_utils.h>

using namespace std::chrono;

namespace cybil
{

	int VkMap[] =
	{
		0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D,
		0x4E, 0x4F, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A,
		0x30, 0x31, 0x32, 0x33, 0x34,
		0x35, 0x36, 0x37, 0x38, 0x39,
		VK_NUMPAD0, VK_NUMPAD1, VK_NUMPAD2, VK_NUMPAD3, VK_NUMPAD4,
		VK_NUMPAD5, VK_NUMPAD6, VK_NUMPAD7, VK_NUMPAD8, VK_NUMPAD9,
		VK_F1, VK_F2, VK_F3, VK_F4, VK_F5, VK_F6,
		VK_F7, VK_F8, VK_F9, VK_F10, VK_F11, VK_F12,
		VK_UP, VK_LEFT, VK_DOWN, VK_RIGHT, VK_PRIOR, VK_NEXT,
		VK_LSHIFT, VK_RSHIFT, VK_LMENU, VK_RMENU, VK_LCONTROL, VK_RCONTROL,
		VK_TAB, VK_END, VK_HOME, VK_DELETE, VK_CAPITAL, VK_LWIN,
		VK_INSERT, VK_SPACE, VK_BACK, VK_RETURN, VK_ESCAPE,
		VK_PAUSE, VK_MENU, VK_ADD, VK_DIVIDE, VK_SUBTRACT, VK_MULTIPLY,
		VK_OEM_COMMA, VK_OEM_PERIOD, VK_OEM_2, VK_OEM_102, VK_OEM_1,
		VK_OEM_7, VK_OEM_4, VK_OEM_6, VK_OEM_3,
		VK_OEM_MINUS, VK_OEM_PLUS, VK_PRINT
	};

	LRESULT CALLBACK Window::proxyWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		Window *window = nullptr;

		if (message == WM_CREATE)
		{
			LPCREATESTRUCT cs = reinterpret_cast<LPCREATESTRUCT>(lParam);
			void *cp = cs->lpCreateParams;
			Window *w = reinterpret_cast<Window *>(cp);
			SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)(w));
		}

		if (LONG_PTR data = GetWindowLongPtr(hWnd, GWLP_USERDATA))
			window = reinterpret_cast<Window *>(data);

		if (window != nullptr)
			return window->windowProc(hWnd, message, wParam, lParam);
		else return DefWindowProc(hWnd, message, wParam, lParam);
	}

	LRESULT CALLBACK Window::windowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		if (message == WM_DESTROY)
			destroy();
		else if (message == WM_PAINT)
		{
			PAINTSTRUCT ps;
			BeginPaint(_window, &ps);

			if (_oglContext && _drawFunction != nullptr)
			{
				glClearColor(0, 0, 0, 1);

				glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

				glMatrixMode(GL_MODELVIEW);
				glLoadIdentity();
				glDisable(GL_DEPTH_TEST);
				glColor4ub(255, 255, 255, 255);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

				auto now = high_resolution_clock::now();
				_drawFunction(*this, duration_cast<milliseconds>(now - _lastTime).count());
				_lastTime = now;

				glEnable(GL_DEPTH_TEST);
				
				SwapBuffers(_windowDc);
			}

			EndPaint(_window, &ps);

			return 0;
		}
		else if (message == WM_CREATE)
		{
			PIXELFORMATDESCRIPTOR pfd =
			{
				sizeof(PIXELFORMATDESCRIPTOR),
				1,
				PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
				PFD_TYPE_RGBA,
				32,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				16,
				0, 0, 0, 0, 0, 0, 0
			};

			_window = hWnd;
			_windowDc = GetDC(hWnd);
			int pfIndex = ChoosePixelFormat(_windowDc, &pfd);
			SetPixelFormat(_windowDc, pfIndex, &pfd);

			_oglContext = wglCreateContext(_windowDc);
			wglMakeCurrent(_windowDc, _oglContext);

			RECT rect;
			GetClientRect(hWnd, &rect);

			glViewport(0, 0, rect.right, rect.bottom);

			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();

			glOrtho(0, rect.right, rect.bottom, 0, -1, 1);

			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			glDisable(GL_DEPTH_TEST);

			return 0;
		}
		else if (message == WM_LBUTTONDOWN || message == WM_RBUTTONDOWN || message == WM_MBUTTONDOWN)
		{
			int mx = LOWORD(lParam);
			int my = HIWORD(lParam);
			if (mx & 1 << 15) mx -= (1 << 16);
			if (my & 1 << 15) my -= (1 << 16);

			MouseButton button;

			if (message == WM_LBUTTONDOWN)
				button = MouseButton::Left;
			else if (message == WM_RBUTTONDOWN)
				button = MouseButton::Right;
			else button = MouseButton::Middle;

			if (_eventFunction != nullptr)
			{
				_eventFunction(Event(EventType::MouseButtonDown, Vector(mx, my), button));

				if(button == MouseButton::Left && _isEmulatingSingleTouch)
				{
					_emulatedTouchId = 1;
					_eventFunction(Event(EventType::TouchBegan, Vector(mx, my), _emulatedTouchId));
				}
			}
			
			PostMessage(hWnd, WM_PAINT, 0, 0);
		}
		else if(message == WM_KILLFOCUS && _isEmulatingSingleTouch && _emulatedTouchId != 0)
			_emulatedTouchId = 0;
		else if (message == WM_LBUTTONUP || message == WM_RBUTTONUP || message == WM_MBUTTONUP)
		{
			int mx = LOWORD(lParam);
			int my = HIWORD(lParam);
			if (mx & 1 << 15) mx -= (1 << 16);
			if (my & 1 << 15) my -= (1 << 16);

			MouseButton button;

			if (message == WM_LBUTTONUP)
				button = MouseButton::Left;
			else if (message == WM_RBUTTONUP)
				button = MouseButton::Right;
			else button = MouseButton::Middle;

			if (_eventFunction != nullptr)
			{
				_eventFunction(Event(EventType::MouseButtonUp, Vector(mx, my), button));

				if(button == MouseButton::Left && _isEmulatingSingleTouch)
				{
					_eventFunction(Event(EventType::TouchEnded, Vector(mx, my), _emulatedTouchId));
					_emulatedTouchId = 0;
				}
			}

			PostMessage(hWnd, WM_PAINT, 0, 0);
		}
		else if(message == WM_MOUSEWHEEL)
		{
			MOUSEHOOKSTRUCTEX *hook = (MOUSEHOOKSTRUCTEX *)lParam;
			short delta = HIWORD(wParam);

			if(_eventFunction != nullptr)
				_eventFunction(Event(EventType::MouseWheelScrolled, delta));
			
			PostMessage(hWnd, WM_PAINT, 0, 0);
		}
		else if(message == WM_KEYDOWN || message == WM_KEYUP)
		{
			char c = MapVirtualKey(wParam, MAPVK_VK_TO_CHAR);
			
			if(_eventFunction != nullptr) {
				int vkIndex = -1;
				for(int i = 0; i < sizeof(VkMap) / sizeof(VkMap[0]); i++)
				{
					if(VkMap[i] == wParam)
					{
						vkIndex = i;
						break;
					}
				}

				if(vkIndex != -1)
					_eventFunction(Event(message == WM_KEYDOWN ? EventType::KeyDown : EventType::KeyUp, (Key)vkIndex));

				if(message == WM_KEYDOWN && isprint(c))
					_eventFunction(Event(EventType::TextEntered, (unsigned char)c));
			}

			PostMessage(hWnd, WM_PAINT, 0, 0);
		}
		else if (message == WM_MOUSEMOVE)
		{
			int mx = LOWORD(lParam);
			int my = HIWORD(lParam);
			if (mx & 1 << 15) mx -= (1 << 16);
			if (my & 1 << 15) my -= (1 << 16);

			if (_eventFunction != nullptr)
			{
				_eventFunction(Event(EventType::MouseMoved, Vector(mx, my)));

				if(_isEmulatingSingleTouch && _emulatedTouchId != 0)
					_eventFunction(Event(EventType::TouchMoved, Vector(mx, my), _emulatedTouchId));
			}

			PostMessage(hWnd, WM_PAINT, 0, 0);
		}
		else if(message == WM_SIZE)
		{
			int width = LOWORD(lParam);
			int height = HIWORD(lParam);

			if(_eventFunction != nullptr)
				_eventFunction(Event(EventType::WindowResized, Size(width, height)));

			PostMessage(hWnd, WM_PAINT, 0, 0);
		}
		else if(message == WM_CLOSE)
		{
			if(_eventFunction != nullptr)
				_eventFunction(Event(EventType::WindowClosed));
		}

		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	Window::Window() : WindowProtocol()
	{
		_windowConfig.windowTitle = "Cybil";
		_windowConfig.surfaceSize = Size(800, 600);
		_windowConfig.bitsPerPixel = 32;
		_windowConfig.windowStyle = static_cast<WindowStyle>(Resize | Minimize | Close | Title);

		create(_windowConfig);
	}

	Window::Window(const WindowConfiguration &configuration) : WindowProtocol(configuration)
	{
		create(configuration);
	}

	Window::~Window()
	{
		destroy();
	}

	void Window::create(const cybil::WindowConfiguration &configuration)
	{
		WNDCLASSEX wc = {};

		wc.cbSize = sizeof(WNDCLASSEX);
		wc.hInstance = GetModuleHandle(NULL);
		wc.lpfnWndProc = &Window::proxyWindowProc;
		wc.hbrBackground = (HBRUSH)COLOR_BACKGROUND;
		wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
		wc.lpszClassName = "CYBILAPP";
		wc.hIcon = LoadIcon(0, IDI_APPLICATION);
		wc.hCursor = LoadCursor(0, IDC_ARROW);
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;

		DWORD style = WS_SYSMENU;

		if (!(configuration.windowStyle & Close))
			wc.style |= CS_NOCLOSE;
		if (configuration.windowStyle & Resize)
			style |= WS_THICKFRAME | WS_MAXIMIZEBOX;
		if (configuration.windowStyle & Title)
			style |= WS_CAPTION;
		if (configuration.windowStyle & Border)
			style |= WS_BORDER;
		if (configuration.windowStyle & Minimize)
			style |= WS_MINIMIZEBOX;

		RegisterClassEx(&wc);

		_emulatedTouchId = 0;

		int windowWidth = configuration.surfaceSize.width;
		int windowHeight = configuration.surfaceSize.height;

		if(!(configuration.windowStyle & FullScreen))
		{
			windowWidth += 16;
			windowHeight += 39;
		}

		FileUtils::getInstance()->setCacheDir("assets/");

		CreateWindow(wc.lpszClassName, configuration.windowTitle.c_str(), style, 100, 100, windowWidth, windowHeight, 0, 0, wc.hInstance, this);
	}

	void Window::start()
	{
		ShowWindow(_window, SW_SHOW);
		UpdateWindow(_window);

		_lastTime = high_resolution_clock::now();

		MSG msg;
		while (GetMessage(&msg, 0, 0, 0))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		destroy();
	}

	void Window::stop()
	{
		PostQuitMessage(0);
	}

	void Window::destroy()
	{
		if (_oglContext != nullptr)
		{
			wglMakeCurrent(_windowDc, 0);
			wglDeleteContext(_oglContext);
			PostQuitMessage(0);

			_oglContext = nullptr;
		}
	}

}
#endif
