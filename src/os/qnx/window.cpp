#include <cybil/os.h>

#ifdef CYBIL_TARGET_QNX

#include <cybil/os/qnx/window.h>
#include <cybil/os/qnx/gl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/platform.h>

namespace cybil
{

	Window::Window() : WindowProtocol()
	{
		_windowConfig.windowTitle = "Cybil";
		_windowConfig.surfaceSize = Size(800, 600);
		_windowConfig.bitsPerPixel = 32;
		_windowConfig.windowStyle = static_cast<WindowStyle>(Resize | Minimize | Close | Title);

		create(_windowConfig);
	}

	Window::Window(const WindowConfiguration &configuration) : WindowProtocol(configuration)
	{
		create(configuration);
	}

	Window::~Window()
	{
		destroy();
	}

	void Window::create(const cybil::WindowConfiguration &configuration)
	{
	    int resultCode, numConfigs, angle;
        int format = SCREEN_FORMAT_RGBX8888;
        const int usage = SCREEN_USAGE_OPENGL_ES1 | SCREEN_USAGE_ROTATION;

        EGLint attributesList[] = {
                EGL_RED_SIZE, 8,
                EGL_GREEN_SIZE, 8,
                EGL_BLUE_SIZE, 8,
                EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
                EGL_RENDERABLE_TYPE, EGL_OPENGL_ES_BIT,
                EGL_NONE // End of attributes
        };

        screen_create_context(&_screenContext, 0);
        screen_create_window(&_window, _screenContext);

        _eglDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
        resultCode = eglInitialize(_eglDisplay, 0, 0);
        resultCode = eglBindAPI(EGL_OPENGL_ES_API);

        eglChooseConfig(_eglDisplay, attributesList, &_eglConfig, 1, &numConfigs);
        _eglContext = eglCreateContext(_eglDisplay, _eglConfig, EGL_NO_CONTEXT, 0);

        bps_initialize();
        navigator_request_events(0);
        screen_request_events(_screenContext);
        navigator_rotation_lock(false);

        screen_set_window_property_iv(_window, SCREEN_PROPERTY_USAGE, &usage);

        int screenRect[4] = { 0, 0, 0, 0 };

        screen_get_window_property_pv(_window, SCREEN_PROPERTY_RENDER_BUFFERS, (void **) &_screenBuffer);
        screen_get_window_property_iv(_window, SCREEN_PROPERTY_BUFFER_SIZE, screenRect + 2);
        screen_set_window_property_iv(_window, SCREEN_PROPERTY_FORMAT, &format);
        screen_set_window_property_iv(_window, SCREEN_PROPERTY_USAGE, &usage);
        screen_get_window_property_pv(_window, SCREEN_PROPERTY_DISPLAY, (void **) &_display);
        screen_get_window_property_pv(_window, SCREEN_PROPERTY_MODE, (void **) &_displayMode);

        angle = atoi(getenv("ORIENTATION"));

        int width = atoi(getenv("WIDTH"));
        int height = atoi(getenv("HEIGHT"));
        int bufferSize[2] = { width, height };

        screen_set_window_property_iv(_window, SCREEN_PROPERTY_BUFFER_SIZE, bufferSize);
        screen_set_window_property_iv(_window, SCREEN_PROPERTY_ROTATION, &angle);

        screen_create_window_buffers(_window, 2);

        screen_post_window(_window, _screenBuffer, 1, screenRect, 0);

        _eglSurface = eglCreateWindowSurface(_eglDisplay, _eglConfig, _window, 0);
        resultCode = eglMakeCurrent(_eglDisplay, _eglSurface, _eglSurface, _eglContext);
        resultCode = eglSwapInterval(_eglDisplay, 1);

        EGLint surfaceWidth, surfaceHeight;
        eglQuerySurface(_eglDisplay, _eglSurface, EGL_WIDTH, &surfaceWidth);
        eglQuerySurface(_eglDisplay, _eglSurface, EGL_HEIGHT, &surfaceHeight);

        glViewport(0, 0, surfaceWidth, surfaceHeight);

        _projection = (glMatrix *) malloc(sizeof(glMatrix));

        glLoadIdentity(_projection);
        gluPerspective(_projection, 45, (float)surfaceWidth / (float)surfaceHeight, 1, 200);
	}

	void Window::start()
	{
	    bool quit = false;
	    while(!quit)
        {
            int result, domain;
            bps_event_t *event = 0;
            result = bps_get_event(&event, -1);

            if(event)
            {
                domain = bps_event_get_domain(event);
                if(domain == navigator_get_domain())
                {
                    switch(bps_event_get_code(event))
                    {
                        case NAVIGATOR_EXIT:
                            quit = true;
                            break;
                        default:
                            break;
                    }
                }
                else if(domain == screen_get_domain())
                {
                    screen_event_t screenEvent = screen_event_get_event(event);
                    int value;

                    screen_get_event_property_iv(screenEvent, SCREEN_PROPERTY_TYPE, &value);

                    switch(value)
                    {
                        case SCREEN_EVENT_MTOUCH_TOUCH:
                            break;
                        default:
                            break;
                    }
                }
            }

            if (_drawFunction != nullptr)
                _drawFunction(1);

            eglSwapBuffers(_eglDisplay, _eglSurface);
        }

		destroy();
	}

	void Window::destroy()
	{
	    if(_eglDisplay != EGL_NO_DISPLAY)
        {
            eglMakeCurrent(_eglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
            if(_eglSurface != EGL_NO_SURFACE)
            {
                eglDestroySurface(_eglDisplay, _eglSurface);
                _eglSurface = EGL_NO_SURFACE;
            }

            if(_eglContext != EGL_NO_CONTEXT)
            {
                eglDestroyContext(_eglDisplay, _eglContext);
                _eglContext = EGL_NO_CONTEXT;
            }
        }

        free(_projection);

        screen_stop_events(_screenContext);
        bps_shutdown();

        screen_destroy_window(_window);
        screen_destroy_context(_screenContext);
	}

}
#endif
