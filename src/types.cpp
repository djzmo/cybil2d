#include <cybil/types.h>

namespace cybil
{

Vector &Vector::operator+(const Vector &right)
{
    x += right.x;
    y += right.y;
    
    return *this;
}

Vector &Vector::operator-(const Vector &right)
{
    x -= right.x;
    y -= right.y;
    
    return *this;
}

const Color Color::Black = Color(0, 0, 0, 255);
const Color Color::Gray = Color(128, 128, 128, 255);
const Color Color::White = Color(255, 255, 255, 255);
const Color Color::Red = Color(255, 0, 0, 255);
const Color Color::Green = Color(0, 255, 0, 255);
const Color Color::Blue = Color(0, 0, 255, 255);
const Color Color::Orange = Color(255, 140, 0, 255);
const Color Color::Purple = Color(0, 255, 255, 255);
const Color Color::Transparent = Color(0, 0, 0, 0);

}