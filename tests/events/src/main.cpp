#include <cybil/cybil.h>
#include <string>
#include <algorithm>

using namespace cybil;

std::string debugStr;
Text debugText;
Font defaultFont;

char *keyStr[] =
{
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
    "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
    "Number0", "Number1", "Number2", "Number3", "Number4",
    "Number5", "Number6", "Number7", "Number8", "Number9",
    "Numpad0", "Numpad1", "Numpad2", "Numpad3", "Numpad4",
    "Numpad5", "Numpad6", "Numpad7", "Numpad8", "Numpad9",
    "F1", "F2", "F3", "F4", "F5", "F6",
    "F7", "F8", "F9", "F10", "F11", "F12",
    "UpArrow", "LeftArrow", "DownArrow", "RightArrow", "PageUp", "PageDown",
    "LShift", "RShift", "LAlt", "RAlt", "LCtrl", "RCtrl",
    "Tab", "End", "Home", "Delete", "CapsLock", "System",
    "Insert", "Space", "BackSpace", "Enter", "Escape",
    "Pause", "Menu", "Add", "Divide", "Subtract", "Multiply",
    "Comma", "Period", "Slash", "BackSlash", "SemiColon",
    "SingleQuote", "LBracket", "RBracket", "Tilde",
    "Dash", "Equal", "PrintScreen"
};

void appendLog(const std::string &text)
{
	if(std::count(debugStr.begin(), debugStr.end(), '\n') < 16)
		debugStr += "\n" + text;
	else debugStr = debugStr.substr(debugStr.find('\n') + 1) + "\n" + text;

	printf("%s\n", text.c_str());

	debugText.setCaption(debugStr);
}

void draw(const DrawTarget &target, double delta)
{
	target.draw(debugText);
}

void processEvent(const Event &event)
{
	if(event.type == KeyDown || event.type == KeyUp)
	{
		std::string type = event.type == KeyDown ? "KeyDown" : "KeyUp";

		appendLog(type + " " + keyStr[event.key]);
	}
	else if(event.type == TextEntered)
		appendLog(std::string("TextEntered ") + (char)event.charCode);
	else if(event.type == MouseButtonDown || event.type == MouseButtonUp)
	{
		std::string type = event.type == MouseButtonDown ? "MouseButtonDown" : "MouseButtonUp";
		std::string button = "Middle";

		if(event.mouseButton == Left)
			button = "Left";
		else if(event.mouseButton == Right)
			button = "Right";

		appendLog(type + " " + button);
	}
	else if(event.type == MouseMoved)
	{
		char buffer[64];
		sprintf(buffer, "MouseMoved (%d, %d)", (int)event.mousePosition.x, (int)event.mousePosition.y);
		appendLog(buffer);
	}
	else if(event.type == MouseWheelScrolled)
	{
		char buffer[64];
		sprintf(buffer, "MouseWheelScrolled (%d)", event.mouseWheelDelta);
		appendLog(buffer);
	}
	else if(event.type == TouchBegan || event.type == TouchMoved || event.type == TouchEnded)
	{
		std::string type = "TouchMoved";

		if(event.type == TouchBegan)
			type = "TouchBegan";
		else if(event.type == TouchEnded)
			type = "TouchEnded";

		char buffer[64];
		sprintf(buffer, "%s ID:%d (%d, %d)", type.c_str(), event.touchId, (int)event.touchPosition.x, (int)event.touchPosition.y);
		appendLog(buffer);
	}
	else if(event.type == WindowResized)
	{
		char buffer[64];
		sprintf(buffer, "WindowResized (%d, %d)", (int)event.windowSize.width, (int)event.windowSize.height);
		appendLog(buffer);
	}
}

int main(int argc, char **argv)
{
    WindowConfiguration conf("Tests - Events", cybil::Size(960, 540));
    Window window(conf);

    defaultFont.createFromFile("Bebas.ttf");
    debugText.setFont(&defaultFont);
    debugText.setPosition(Vector(10, 10));
	debugText.setSize(32);

    window.setDrawFunction(draw);
    window.setEventFunction(processEvent);
    window.start();

    return 0;
}
