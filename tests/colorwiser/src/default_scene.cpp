#include "default_scene.h"

DefaultScene::DefaultScene()
: Scene()
{
	_defaultFont.createFromFile("Bebas.ttf");
	_title.setFont(&_defaultFont);
	_title.setCaption("ColorWiser");
	_title.setPosition(Vector(100, 100));
	_title.setColor(Color::White);
    _title.setSize(32);

	add(&_title);
}

void DefaultScene::processEvent(const Event &event) const
{

}