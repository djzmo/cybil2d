#ifndef _DEFAULTSCENE_H_
#define _DEFAULTSCENE_H_

#include "scene.h"

class DefaultScene : public Scene
{
	Font _defaultFont;
	Text _title;
	Text _startButton;

public:
	DefaultScene();

	void processEvent(const Event &event) const;
};

#endif