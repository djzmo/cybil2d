#ifndef _APPLICATION_H_
#define _APPLICATION_H_

#include <cybil/cybil.h>

using namespace cybil;

class Scene;    
class Application
{
    static const char *WINDOW_TITLE;
    
    Window _window;
    bool _isFullScreen;
    
public:
    Application(Scene *initialScene, unsigned int screenWidth, unsigned int screenHeight, bool fullScreen = false);
    ~Application();

	void draw(const DrawTarget &target, double delta);
	void processEvent(const Event &event);

    void run();
};

#endif