#include "director.h"
#include "scene.h"

Director Director::_instance;

Director::Director()
{
    _currentScene = nullptr;
}

Director *Director::instance()
{
    return &_instance;
}

Window *Director::getWindow()
{
    return _window;
}

Size Director::getWindowSize()
{
    if(_window != nullptr)
		return _window->getConfiguration().surfaceSize;
    else return Size(0, 0);
}

Vector Director::getCenterPoint()
{
    return Vector(_window->getConfiguration().surfaceSize.width / 2, _window->getConfiguration().surfaceSize.height / 2);
}

void Director::setScene(Scene *scene)
{
    if(_currentScene != nullptr)
        delete _currentScene;
    
    _currentScene = scene;
}

Scene *Director::getScene()
{
    return _currentScene;
}

void Director::abort()
{
    if(_window != nullptr)
        _window->stop();
}
