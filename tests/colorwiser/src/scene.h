#ifndef _SCENE_H_
#define _SCENE_H_

#include "drawable_container.h"

class Scene : public DrawableContainer
{
public:
    Scene() : DrawableContainer() {}

	virtual void processEvent(const Event &event) const {}
};

#endif