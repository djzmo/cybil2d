#ifndef _DIRECTOR_H_
#define _DIRECTOR_H_

#include <cybil/cybil.h>

using namespace cybil;
    
class Application;
class Scene;
class Director
{
    friend class Application;
    
    static Director _instance;
    Window *_window;
    Scene *_currentScene;
    
    Director();
public:
    static Director *instance();
    
    Window *getWindow();
    Size getWindowSize();
    Vector getCenterPoint();
    void setScene(Scene *scene);
    Scene *getScene();
    void abort();
};

#endif