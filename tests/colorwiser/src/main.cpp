#include <cybil/cybil.h>

using namespace cybil;

const int SCREEN_WIDTH = 960, SCREEN_HEIGHT = 540;

Window *window;
Font defaultFont;
Text title, title2, touchToStart;
Text scoreText, gameOver;
int state = 0;
int score = 0;
int answer = 0;
int boxes = 3;
double remaining = 60000; // 60k ms = 60 s = 1 m
RectShape colors[7];

// State 0: Menu utama
// State 1: Permainan
// State 2: Skor

int randomBetween(int min, int max)
{
	return rand () % (max - min + 1) + min;
}

void generateLevel()
{
	if(score >= 75)
		boxes = 7;
	else if(score >= 40)
		boxes = 6;
	else if(score >= 25)
		boxes = 5;
	else if(score >= 10)
		boxes = 4;
	else boxes = 3;
	
	Color colorStock[] = { Color::Orange, Color::Red, Color::Gray, Color::Green, Color::Purple };
	int innocentNum = randomBetween(0, 4);
	int culpritNum = innocentNum;
	while(culpritNum == innocentNum)
		culpritNum = randomBetween(0, 4);
	Color innocent = colorStock[innocentNum];
	Color culprit = colorStock[culpritNum];
	answer = randomBetween(0, boxes - 1);

	for(int i = 0; i < boxes; i++)
	{
		colors[i] = Shapes::rect(Vector(10 + i * SCREEN_WIDTH / boxes, 10), Size(SCREEN_WIDTH / boxes - 20, SCREEN_HEIGHT - 20));
		if(i == answer)
			colors[i].setFillColor(culprit);
		else colors[i].setFillColor(innocent);
	}
}

void draw(const DrawTarget &target, double delta)
{
	if(state == 0)
	{
		target.draw(title);
		target.draw(title2);
		target.draw(touchToStart);
	}
	else if(state == 1)
	{
		for(int i = 0; i < boxes; i++)
			target.draw(colors[i]);
		target.draw(scoreText);
	}
	else if(state == 2)
	{
		target.draw(gameOver);
		target.draw(scoreText);
	}
}

void processEvent(const Event &event)
{
	if(event.type == TouchEnded)
	{
        if(state == 0)
		{
			score = 0;
            state = 1;
			remaining = 60000;
			generateLevel();
			scoreText.setCaption("0");
			scoreText.setColor(Color::Black);
			scoreText.setPosition(Vector(430, 20));
		}
        else if(state == 1)
        {
			Vector pos = event.touchPosition;
			int boxWidth = SCREEN_WIDTH / boxes;
            for(int i = 0; i < boxes; i++)
			{
				if(pos.x >= i * boxWidth && pos.x < i * boxWidth + boxWidth)
				{
					if(i == answer)
					{
						score++;
						char buffer[16];
						sprintf(buffer, "%d", score);
						scoreText.setCaption(buffer);
						generateLevel();
					}
					else
					{
						state = 2;
						scoreText.setColor(Color::White);
						scoreText.setPosition(Vector(430, 450));
					}
				}
			}
        }
	}
	else if(event.type == KeyUp && event.key == Key::Escape)
	{
	    if(state == 0)
	    {
            window->stop();
            delete window;
        }
        else if(state == 1 || state == 2)
            state = 0;
	}
}

int main(int argc, char **argv)
{
	srand(time(0));

    WindowConfiguration conf("ColorWiser", cybil::Size(SCREEN_WIDTH, SCREEN_HEIGHT));
    window = new Window(conf);

    defaultFont.createFromFile("Bebas.ttf");
	title.setCaption("Color");
	title.setFont(&defaultFont);
	title.setPosition(Vector(100, 50));
	title.setColor(Color::Orange);
	title.setSize(200);
	title2.setCaption("Wiser");
	title2.setFont(&defaultFont);
	title2.setPosition(Vector(470, 50));
	title2.setColor(Color::White);
	title2.setSize(200);
	touchToStart.setSize(60);
	touchToStart.setFont(&defaultFont);
	touchToStart.setPosition(Vector(320, 350));
	touchToStart.setCaption("Touch to start");
	scoreText.setCaption("0");
	scoreText.setSize(60);
	scoreText.setColor(Color::Black);
	scoreText.setFont(&defaultFont);
	gameOver.setCaption("Game Over");
	gameOver.setSize(200);
	gameOver.setColor(Color::Orange);
	gameOver.setPosition(Vector(140, 100));
	gameOver.setFont(&defaultFont);

    window->setDrawFunction(draw);
    window->setEventFunction(processEvent);
	window->setEmulateSingleTouch(true);
    window->start();

    return 0;
}