#ifndef _DRAWABLECONTAINER_H_
#define _DRAWABLECONTAINER_H_

#include <cybil/cybil.h>

using namespace cybil;

class DrawableContainer : public Drawable
{
protected:
    std::vector<Drawable *> _drawables;
    
    virtual void drawChilds(const DrawTarget &target) const;

public:
    DrawableContainer();
    virtual ~DrawableContainer();
    
	virtual void add(Drawable *drawable);
	virtual void remove(Drawable *drawable);
    virtual void removeAll();
    virtual bool hasChild(Drawable *drawable);
    virtual unsigned int countChilds();
    virtual void draw(const DrawTarget &target) const;
};

#endif