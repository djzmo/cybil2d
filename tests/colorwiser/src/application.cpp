#include "application.h"
#include "director.h"
#include "scene.h"

const char *Application::WINDOW_TITLE = "Cybil";
Application *self = nullptr;

Application::Application(Scene *initialScene, unsigned int screenWidth, unsigned int screenHeight, bool fullScreen) :
	_window(WindowConfiguration(WINDOW_TITLE, cybil::Size(screenWidth, screenHeight), 32, 60,
		fullScreen ? static_cast<WindowStyle>(FullScreen) : static_cast<WindowStyle>(Resize | Minimize | Border | Title | Close)))
{
    _isFullScreen = fullScreen;
    
    Director::instance()->setScene(initialScene);
    Director::instance()->_window = &_window;

	self = this;
}

Application::~Application()
{
}

void drawFunctionWrapper(const DrawTarget &target, double delta)
{
	if(self != nullptr)
		self->draw(target, delta);
}

void eventFunctionWrapper(const Event &event)
{
	if(self != nullptr)
		self->processEvent(event);
}

void Application::draw(const DrawTarget &target, double delta)
{
    Director *director = Director::instance();
	if(director->getScene() != nullptr)
		director->getScene()->draw(target);
}

void Application::processEvent(const Event &event)
{
    Director *director = Director::instance();
	if(director->getScene() != nullptr)
		director->getScene()->processEvent(event);
}

void Application::run()
{    
	_window.setDrawFunction(&drawFunctionWrapper);
	_window.setEventFunction(&eventFunctionWrapper);
    _window.start();
}
