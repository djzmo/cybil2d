#include "drawable_container.h"
    
DrawableContainer::DrawableContainer()
{
}

DrawableContainer::~DrawableContainer()
{
    removeAll();
}

void DrawableContainer::add(Drawable *drawable)
{
    if(drawable == nullptr)
        return;
    
    _drawables.push_back(drawable);
}

void DrawableContainer::remove(Drawable *drawable)
{
    if(drawable == nullptr)
        return;
    
    std::vector<Drawable *>::size_type i;
    for(i = 0; i < _drawables.size(); i++)
    {
        if(_drawables.at(i) == drawable)
        {
            delete _drawables.at(i);
            _drawables.erase(_drawables.begin() + i);
            break;
        }
    }
}
    
void DrawableContainer::removeAll()
{
    std::vector<Drawable *>::size_type i;
    for(i = 0; i < _drawables.size(); i++)
    {
        delete _drawables.at(i);
    }
    
    _drawables.clear();
}

bool DrawableContainer::hasChild(Drawable *drawable)
{
    if(drawable == nullptr)
        return false;
    
    for(std::vector<DrawableContainer *>::size_type i = 0; i < _drawables.size(); i++)
        if(_drawables.at(i) == drawable)
            return true;
    
    return false;
}

unsigned int DrawableContainer::countChilds()
{
    return _drawables.size();
}

void DrawableContainer::drawChilds(const DrawTarget &target) const
{
    for(std::vector<DrawableContainer *>::size_type i = 0; i < _drawables.size(); i++)
        target.draw(*_drawables.at(i));
}

void DrawableContainer::draw(const DrawTarget &target) const
{    
    drawChilds(target);
}
