#include <cybil/cybil.h>

using namespace cybil;

Font fonts[5];
Text texts[5];

void draw(const DrawTarget &target, double delta)
{
	for(int i = 0; i < 5; i++)
		target.draw(texts[i]);
}

void processEvent(const Event &event)
{
}

int main(int argc, char **argv)
{
    WindowConfiguration conf("Tests - Fonts", cybil::Size(960, 540));
    Window window(conf);

    fonts[0].createFromFile("Bebas.ttf");
    fonts[1].createFromFile("OldLondon.ttf");
    fonts[2].createFromFile("EnglishTowne.ttf");
    fonts[3].createFromFile("Philosopher.ttf");
    fonts[4].createFromFile("Guapos.ttf");

	char caption[64];
    for(int i = 0; i < 5; i++)
    {
		sprintf(caption, "HELLO WORLD %d", i);
		texts[i].setCaption(caption);
    	texts[i].setFont(&fonts[i]);
    	texts[i].setSize(48);
    	texts[i].setPosition(Vector(10, 50 + i * 75));
    }

    window.setDrawFunction(draw);
    window.setEventFunction(processEvent);
    //window.setAcceptsMouseMovedEvents(true);
    window.start();

    return 0;
}