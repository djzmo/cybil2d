#include <cybil/cybil.h>

using namespace cybil;

Font font;
Text texts[5];

void draw(const DrawTarget &target, double delta)
{
	for(int i = 0; i < 5; i++)
		target.draw(texts[i]);
}

void processEvent(const Event &event)
{
}

int main(int argc, char **argv)
{
    WindowConfiguration conf("Tests - Texts", cybil::Size(960, 540));
    Window window(conf);

    font.createFromFile("Bebas.ttf");

	char caption[64];
    for(int i = 0; i < 5; i++)
    {
		sprintf(caption, "Hello world %d", i);
		texts[i].setCaption(caption);
    	texts[i].setFont(&font);
    	texts[i].setSize(40 + i * 10);
    	texts[i].setPosition(Vector(10, 20 + i * 75));
    }

    texts[0].setColor(Color::Red);
    texts[1].setColor(Color::Green);
    texts[2].setColor(Color::Blue);
    texts[3].setColor(Color::Orange);
    texts[4].setColor(Color::Purple);

    window.setDrawFunction(draw);
    window.setEventFunction(processEvent);
    window.setAcceptsMouseMovedEvents(true);
    window.start();

	return 0;
}