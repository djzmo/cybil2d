package com.djzmo.skripsi.cybilapp;

import android.content.res.AssetManager;
import android.os.Environment;
import android.os.Bundle;
import android.util.Log;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGLView = new CybilSurfaceView(this);
        setContentView(mGLView);

        copyAssets();
        setCachePath(Environment.getExternalStorageDirectory() + "/Android/data/" + getPackageName() + "/cache/");
        nativeInit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGLView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGLView.onResume();
    }

    @Override
    public void onBackPressed() {
        nativeBack();
    }

    public void jniFinish() {
        finish();
    }

    private void copyAssets() {
        AssetManager assetManager = getAssets();
        String[] files = null;
        try {
            files = assetManager.list("");
        } catch (IOException e) {
            Log.e("tag", "Failed to get asset file list.", e);
        }
        for(String filename : files) {
            if (filename.equals("images")
                    || filename.equals("sounds")
                    || filename.equals("webkit")
                    || filename.equals("databases")
                    || filename.equals("kioskmode"))
                continue;

            InputStream in = null;
            OutputStream out = null;
            try {
                in = assetManager.open(filename);

                String tmpPath = Environment.getExternalStorageDirectory() + "/Android/data/" + getPackageName() + "/cache/";
                File tmpDir = new File(tmpPath);
                if (!tmpDir.exists())
                    tmpDir.mkdirs();

                File outFile = new File(tmpPath, filename);

                out = new FileOutputStream(outFile);
                copyFile(in, out);
                in.close();
                in = null;
                out.flush();
                out.close();
                out = null;
            } catch(IOException e) {
                Log.e("tag", "Failed to copy asset file: " + filename, e);
            }
        }
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

    private GLSurfaceView mGLView;

    static {
        System.loadLibrary("cybilapp");
    }

    private static native void setCachePath(String path);
    private static native void nativeInit();
    private static native void nativeBack();
}

class CybilSurfaceView extends GLSurfaceView {
    public CybilSurfaceView(Context context) {
        super(context);
        mRenderer = new CybilRenderer();
        setRenderer(mRenderer);
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE ||
                        event.getAction() == MotionEvent.ACTION_UP) {
                    for(int i = 0; i < event.getPointerCount(); i++) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN)
                            nativeTouchBegan(event.getPointerId(i), event.getX(i), event.getY(i));
                        else if(event.getAction() == MotionEvent.ACTION_MOVE)
                            nativeTouchMoved(event.getPointerId(i), event.getX(i), event.getY(i));
                        else if(event.getAction() == MotionEvent.ACTION_UP)
                            nativeTouchEnded(event.getPointerId(i), event.getX(i), event.getY(i));
                    }
                }
                return true;
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        nativePause();
    }

    @Override
    public void onResume() {
        super.onResume();
        nativeResume();
    }

    CybilRenderer mRenderer;

    private static native void nativeTouchBegan(int id, float x, float y);
    private static native void nativeTouchMoved(int id, float x, float y);
    private static native void nativeTouchEnded(int id, float x, float y);
    private static native void nativePause();
    private static native void nativeResume();
    private static native void nativeTogglePauseResume();
}

class CybilRenderer implements GLSurfaceView.Renderer {
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
    }

    public void onSurfaceChanged(GL10 gl, int w, int h) {
        nativeResize(w, h);
    }

    public void onDrawFrame(GL10 gl) {
        nativeRender();
    }

    private static native void nativeResize(int w, int h);
    private static native void nativeRender();
    private static native void nativeDone();
}
