#include <cybil/cybil.h>

using namespace cybil;

Texture sorloTex;
Texture gloopTex;
Texture backgroundTex; // subtlepatterns eight horns
Sprite background;
Sprite sorlo; // http://opengameart.org/content/sorlo-a-funny-sorcerer
Sprite gloop[2]; // http://opengameart.org/content/gloop-monster

void draw(const DrawTarget &target, double delta)
{
	target.draw(background);
	target.draw(sorlo);
	target.draw(gloop[0]);
	target.draw(gloop[1]);
}

void processEvent(const Event &event)
{
}

int main(int argc, char **argv)
{
    WindowConfiguration conf("Tests - Sprites", cybil::Size(960, 540));
    Window window(conf);

	backgroundTex.createFromFile("background.png");
    sorloTex.createFromFile("sorlo.png");
    gloopTex.createFromFile("gloop.png");
    sorlo.setTexture(&sorloTex);
    sorlo.setPosition(Vector(100, 100));
	background.setTexture(&backgroundTex);
    gloop[0].setTexture(&gloopTex);
    gloop[0].setTextureRect(Rect(0, 0, 100, 85));
    gloop[0].setPosition(Vector(400, 10));
    gloop[1].setTexture(&gloopTex);
    gloop[1].setTextureRect(Rect(300, 85, 100, 85));
    gloop[1].setPosition(Vector(400, 150));

    window.setDrawFunction(draw);
    window.setEventFunction(processEvent);
    window.setAcceptsMouseMovedEvents(true);
    window.start();

	return 0;
}