#include <cybil/cybil.h>

using namespace cybil;

RectShape background;

void draw(const DrawTarget &target, double delta)
{
	target.draw(background);

	target.draw(Shapes::circle(Vector(200, 300), 100));
	target.draw(Shapes::line(Vector(100, 120), Vector(300, 200), 5));
	target.draw(Shapes::circle(Vector(760, 300), 100));
	target.draw(Shapes::line(Vector(860, 120), Vector(660, 200), 5));

	target.draw(Shapes::circle(Vector(220, 330), 50, Color::Orange));
	target.draw(Shapes::circle(Vector(760, 330), 50, Color::Orange));
}

void processEvent(const Event &event)
{
}

int main(int argc, char **argv)
{
    WindowConfiguration conf("Tests - Shapes", cybil::Size(960, 540));
    Window window(conf);

    background = Shapes::rect(Vector(10, 10), Size(940, 520));
    background.setFillColor(Color::Gray);

    window.setDrawFunction(draw);
    window.setEventFunction(processEvent);
    window.setAcceptsMouseMovedEvents(true);
    window.start();

	return 0;
}