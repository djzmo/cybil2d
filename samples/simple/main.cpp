#include <cybil/cybil.h>
#include <iostream>

using namespace cybil;

void draw(const DrawTarget &target, double delta)
{
}

void processEvent(const Event &event)
{
	if (event.type == MouseButtonDown)
	{
		if (event.mouseButton == MouseButton::Left)
			std::cout << "MouseButtonDown Left\n";
		else if (event.mouseButton == MouseButton::Right)
			std::cout << "MouseButtonDown Right\n";
	}
	else if (event.type == MouseButtonUp)
	{
		if (event.mouseButton == MouseButton::Left)
			std::cout << "MouseButtonUp Left\n";
		else if (event.mouseButton == MouseButton::Right)
			std::cout << "MouseButtonUp Right\n";
	}
	else if (event.type == MouseMoved)
		std::cout << "MouseMoved (" << (int)event.mousePosition.x << ", " << (int)event.mousePosition.y << ")\n";
}

int main(int argc, char **argv)
{
    WindowConfiguration conf("Cybil Demo", cybil::Size(800, 600), 32, 60);
    Window window(conf);
    window.setDrawFunction(draw);
    window.setEventFunction(processEvent);
    window.start();
    
    return 0;
}
